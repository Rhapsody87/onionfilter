import javax.swing.*;
import java.io.File;

public class PingGUI {
    private JTextField fileText;
    private JButton browseButton;
    private JTextField timeoutText;
    private JPanel content;
    private JCheckBox outputNamesCheckBox;
    private File inputFile;

    PingGUI(){
        fileText.setEditable(false);
        browseButton.addActionListener(e->{
            JFileChooser fileChooser = new JFileChooser();
            if(fileChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) {
                inputFile = fileChooser.getSelectedFile();
                fileText.setText(inputFile.getPath());
            }
        });
    }

    JPanel getContent(){
        return content;
    }
    String getTimeout(){
        return timeoutText.getText();
    }
    File getInputFile(){
        return inputFile;
    }
    boolean getChecked(){
        return outputNamesCheckBox.isSelected();
    }
}
