import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

public class MainPing {


    private static int READ_TIMEOUT = 3000;
    private static File inputFile = null;
    private static boolean checked = false;

    public static void main(String[] args)throws IOException{
        JOptionPane.showMessageDialog(null, "As this is a program, I am not responsible for anything you may find.\n"+"\n"+"Happy Hunting!\n"+"-Rhapsody");
        JFrame inputFrame = new JFrame("Link Filter");
        PingGUI pingGUI = new PingGUI();
        inputFrame.setSize(450,300);
        inputFrame.setLocationRelativeTo(null);
        AtomicBoolean closed = new AtomicBoolean(false);
        inputFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        inputFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                synchronized (closed){
                    closed.set(true);
                    closed.notify();
                }
                super.windowClosed(e);
            }
        });


        JButton beginButton = new JButton("Begin Filtering");
        beginButton.addActionListener(e->{
            READ_TIMEOUT = Integer.parseInt(pingGUI.getTimeout());
            inputFile = pingGUI.getInputFile();
            checked = pingGUI.getChecked();
            inputFrame.dispose();
        });
        JPanel inputPanel = pingGUI.getContent();
        inputFrame.add(inputPanel);
        inputFrame.add(BorderLayout.SOUTH, beginButton);
        inputFrame.setVisible(true);
        synchronized (closed){
            while(!closed.get()){
                try {
                    closed.wait();
                }
                catch(Exception ignore){
                }
            }
        }


        JFrame progressFrame = new JFrame("Filtering..");
        progressFrame.setSize(300, 300);
        closed.set(false);
        progressFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        progressFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                synchronized (closed) {
                    closed.set(true);
                    closed.notify();
                }
                super.windowClosed(e);
            }
        });
        ProcessingGUI processingGUI = new ProcessingGUI(inputFile, READ_TIMEOUT, checked);
        progressFrame.setContentPane(processingGUI.getContent());
        progressFrame.setLocationRelativeTo(null);
        progressFrame.setVisible(true);
        processingGUI.start();
        progressFrame.dispose();
        synchronized (closed){
            while(!closed.get()){
                try{
                    closed.wait();
                }
                catch(Exception ignore){

                }
            }
        }

        JOptionPane.showConfirmDialog(null, "Filtering Completed!","Completed",JOptionPane.DEFAULT_OPTION);
        System.exit(0);
    }
}
