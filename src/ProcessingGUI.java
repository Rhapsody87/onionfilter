import javax.swing.*;
import java.io.*;
import java.net.*;

public class ProcessingGUI {
    private JProgressBar progressBar1;
    private JPanel panel1;
    private JLabel currentLink;
    private JLabel filteringLabel;
    private static int READ_TIMEOUT;
    private static File outputFile = new File("Output");
    private File inputFile = null;
    private static SocketAddress torSock = new InetSocketAddress("127.0.0.1", 9050);
    private static Proxy pr = new Proxy(Proxy.Type.SOCKS, torSock);
    private static boolean outputNames = false;


    public ProcessingGUI(File inputFile, int timeout, boolean outputNames)throws IOException {
        outputFile.createNewFile();
        READ_TIMEOUT = timeout;
        this.inputFile = inputFile;
        this.outputNames = outputNames;
    }
    public void start()throws IOException{
        int max = 0;
        try{
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            String line = reader.readLine();
            while(line!=null){
                max++;
                line = reader.readLine();
            }
            progressBar1.setMaximum(max);
            progressBar1.setMinimum(0);
            reader.close();
        }
        catch(FileNotFoundException ex){
            JOptionPane.showMessageDialog(null, "File Not Found!", "Error!", JOptionPane.ERROR_MESSAGE);
        }
        try{
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            String line = reader.readLine();
            float count=0;
            while(line!=null){
                count++;
                progressBar1.setValue((int)(Math.floor(count/max)*100));
                currentLink.setText("Current Link: "+line);
                double progress = (count/max)*100;
                filteringLabel.setText("Filtering: "+(double)Math.round(progress*100)/100+"% ("+count+"/"+max+")");
                checkLink(line);
                line = reader.readLine();
            }
        }
        catch(FileNotFoundException ex){
            JOptionPane.showMessageDialog(null, "If you somehow get this. What the fuck.", "Fuck off", JOptionPane.ERROR_MESSAGE);
        }
    }
    public static void checkLink(String url){
        String result;
        if(!url.startsWith("http://")){
            url = "http://"+url;
        }
        int code;
        try{
            URL siteURL = new URL(url);
            HttpURLConnection connection = (HttpURLConnection)siteURL.openConnection(pr);
            connection.setRequestMethod("GET");
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setConnectTimeout(READ_TIMEOUT);
            connection.connect();

            code = connection.getResponseCode();
            if(code==200){
                result = "-> Green <-\t" + "Code: "+code;
                addLink(url);
            }
            else{
                result = "-> Yellow <-\t" + "Code: "+code;
            }
        }
        catch (Exception e){
            result = "-> Red -<\t" + "Wrong Domain - Exception: "+e.getMessage();
        }
        //To Output to a logfile later
        System.out.println(url + "\t\tStatus:"+result);
    }


    private static void addLink(String input) throws IOException {
        FileWriter writer = new FileWriter(outputFile, true);
        writer.append("\n");
        if(outputNames) {
            writer.append(input+" - "+getURLName(input));
        }
        if(!outputNames){
            writer.append(input);
        }
        writer.close();
    }
    JPanel getContent(){
        return panel1;
    }
    private static String getURLName(String inputURL) throws MalformedURLException {
        SocketAddress torSock = new InetSocketAddress("127.0.0.1", 9050);
        Proxy pr = new Proxy(Proxy.Type.SOCKS, torSock);
        String output = null;
        try{
            URL url = new URL(inputURL);
            HttpURLConnection uc = (HttpURLConnection)url.openConnection(pr);
            String line;
            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            while ((line = in.readLine()) != null) {
                if(line.contains("<title>")){
                    output = (line.substring(line.indexOf("<title>")+7,line.indexOf("</title>")));
                }
            }
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return output;
    }
}
